<table class="infotable">
  <tr>
    <td><?= $category['name'] ?></td>
  </tr>
  <tr>
    <td><?= $category['description'] ?></td>
  </tr>
</table>

<br>

<table class="infotable">
<?php foreach ($products as $product): ?>

    <tr>
        <td>
          <a href="/product.php?productId=<?= $product['id'] ?>">
            <?= $product["name"] ?>
          </a>
        </td>
        <td><?= $product["description"] ?></td>
        <td><?= '$'.$product["price"] ?></td>
        <td><?= $product["quantity"] . " pieces" ?></td>
        <td>
          <a href="/updateProduct.php">Edit</a>
        </td>
    </tr>

<?php endforeach ?>
</table>
