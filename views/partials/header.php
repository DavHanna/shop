<!DOCTYPE html>
<html>
    <head>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="/css/styles.css" rel="stylesheet"/>

        <?php if (isset($title)): ?>
            <title>Shop: <?= $title ?></title>
        <?php else: ?>
            <title>Shop</title>
        <?php endif ?>

        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="/js/scripts.js"></script>
    </head>

    <body>
        <div class="container">

            <div id="top">
                <div>
                    <a href="/"><img alt="Shop" src="/img/logo.png"/></a>
                </div>

                <?php if (!empty($_SESSION["id"])): ?>
                    <ul class="nav nav-pills">
                        <li><a href="/">Categories</a></li>
                        <li><a href="/createProduct.php">Create product</a></li>
                        <li><a href="/createCategory.php">Create Category</a></li>
                        <li><a href="/logout.php"><strong>Log Out</strong></a></li>
                    </ul>
                <?php endif ?>

            </div>

            <div id="middle">
