<?php

    // configuration
    require("../includes/config.php");

    $categoryId = $_GET['categoryId'];

    if (empty($categoryId)) {
      apologize("Please choose the category");
    }
    else {
      $category = query("SELECT * FROM categories WHERE id = $categoryId");
      $category = $category[0];

      $products = query("SELECT * FROM products WHERE category_id = $categoryId");

      render("category.php", ["title" => "Category " . $category['name'], "category" => $category, "products" => $products]);
    }



?>
