<?php

    // configuration
    require("../includes/config.php");

    $productId = $_GET['productId'];

    if (empty($productId)) {
      apologize("Please choose the product");
    }
    else {
      $product = query("SELECT * FROM products WHERE id = $productId");
      $product = $product[0];

      render("product.php", ["title" => "Product " . $product['name'], "product" => $product]);
    }



?>
