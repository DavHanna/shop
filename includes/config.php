<?php

// display errors, warnings, and notices
ini_set("display_errors", true);
error_reporting(E_ALL);

// require the helper functions
require_once('helpers.php');

// start the session
session_start();

// require authentication for all pages except /login.php, /logout.php, and /register.php
// if (!in_array($_SERVER["PHP_SELF"], ["/login.php", "/logout.php", "/register.php"]))
// {
//     if (empty($_SESSION["id"]))
//     {
//         redirect("login.php");
//     }
// }

?>
